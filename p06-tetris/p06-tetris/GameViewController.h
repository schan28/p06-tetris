//
//  GameViewController.h
//  p06-tetris
//

//  Copyright (c) 2016 Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *gameScreen;
@property float gameScreenHeight;
@property float gameScreenWidth;

@property float blockDimension;

@property CGPoint spawnPoint;

@property (strong, nonatomic) IBOutlet UIView *fakeLeftView;
@property (strong, nonatomic) IBOutlet UIView *fakeRightView;


@property (nonatomic,strong) NSMutableArray *tetrisArray;


@end
