//
//  AppDelegate.h
//  p06-tetris
//
//  Created by J on 3/10/16.
//  Copyright © 2016 Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

