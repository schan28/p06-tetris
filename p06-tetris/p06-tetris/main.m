//
//  main.m
//  p06-tetris
//
//  Created by J on 3/10/16.
//  Copyright © 2016 Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
