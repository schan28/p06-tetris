//
//  GameScene.m
//  p06-tetris
//
//  Created by J on 3/10/16.
//  Copyright (c) 2016 Stanley Chan. All rights reserved.
//

#import "GameScene.h"


@implementation GameScene

//FIX THIS
-(void)didMoveToView:(SKView *)view {
    
    /* Setup your scene here */    
    

}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        SKShapeNode *squarePiece = [[SKShapeNode alloc] init];
        //squarePiece.path = [self createSquarePiece:blockDimension];
        squarePiece.path = [self createSquarePiece:15];
        
        squarePiece.fillColor = [SKColor cyanColor];
        //squarePiece.position = spawnPoint;
        squarePiece.position = CGPointMake(25,25);
        
        [self addChild:squarePiece];
        NSLog(@"SQUARE?");
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    

}



- (CGMutablePathRef) createSquarePiece : (float) dimension{
    CGMutablePathRef squarePath = CGPathCreateMutable();
    CGPathMoveToPoint(squarePath, NULL, 0, 0);
    CGPathAddLineToPoint(squarePath, NULL, (dimension * 2), 0);
    CGPathAddLineToPoint(squarePath, NULL, (dimension * 2), (dimension * 2));
        CGPathAddLineToPoint(squarePath, NULL, 0, (dimension * 2));
    CGPathCloseSubpath(squarePath);
    return squarePath;
}






@end
