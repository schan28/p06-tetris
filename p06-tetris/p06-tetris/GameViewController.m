//
//  GameViewController.m
//  p06-tetris
//
//  Created by J on 3/10/16.
//  Copyright (c) 2016 Stanley Chan. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"

@implementation GameViewController

@synthesize tetrisArray;
@synthesize gameScreen;
@synthesize gameScreenHeight;
@synthesize gameScreenWidth;
@synthesize blockDimension;
@synthesize spawnPoint;



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    [self initializeGameBoard];
    
    
    
 
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    SKView * skView = (SKView *)self.view;
    
    if (!skView.scene) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        SKScene * scene = [GameScene sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}


- (void) initializeGameBoard{
    

    gameScreenHeight = CGRectGetHeight(gameScreen.bounds);
    gameScreenWidth = CGRectGetWidth(gameScreen.bounds);
    
    //this may be redundant these two lines since all squares have same dimensions
    float blockWidth = gameScreenWidth / 10;
    float blockHeight = gameScreenHeight / 20;
    
    blockDimension = blockWidth;
    
    NSLog(@"height = %f, width = %f", gameScreenHeight, gameScreenWidth);
    NSLog(@"blockheight = %f, blockwidth = %f", blockHeight, blockWidth);

    
    //used these variables to determine where to draw the blocks
    float currentX = CGRectGetMinX(gameScreen.bounds);
    float currentY = CGRectGetMaxY(gameScreen.bounds);
    
    //create 2D Array
    tetrisArray = [NSMutableArray array];
    for(int i = 0; i < 20; i++){
        //blocks from top left point so we have to move up initially
        currentY -= blockHeight;
        NSMutableArray *innerArray = [NSMutableArray array];
        currentX = CGRectGetMinX(gameScreen.bounds);
        for(int j = 0; j < 10; j++){
            

            //PUT BLOCK OBJECTS HERE LATER
            UILabel *block = [[UILabel alloc] initWithFrame:CGRectMake(currentX, currentY, blockWidth, blockHeight)];
            
            //white 1px border
            block.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:1.0f].CGColor;
            block.layer.borderWidth = 1.0f;
            [gameScreen addSubview:block];
            //place block next to it
            currentX += blockWidth;
            
            //setting spawn location
            if(i == 19 && j == 4){
                spawnPoint = CGPointMake(currentX, currentY);
            }
            
        }
        [tetrisArray addObject:innerArray];
    }
}




    
@end
